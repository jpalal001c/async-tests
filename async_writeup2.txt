-> disclaimer
-> I am Joe. Oh it sasys here - I'm from Cognizant. I just work there. 
-> 8 months nodejs experience and building several tools for connecting various services

-> One of the favorite book: NODEJS PROGRAMMING


So I'm going to talk about the  Callback hell, and how to get out of it using (ASync) library by Caolan

But let's go back to basics, what are callbacks?

Some notes: According to wikipedia:

In computer programming, 
a callback is a piece of executable code that is passed as an argument to other code, which is expected to call back (execute) the argument at some convenient time. 

The invocation may be immediate as in a *synchronous* callback, or it might happen at later time as in an *asynchronous* callback

In Node.JS file operations, the fs.readFile has a callback which is handle asynchronously.

What is the callback hell. 

"The problem in Javascript is that the only way to "freeze" a computation and have the "rest of it" execute latter (asynchronously) is to put "the rest of it" inside a callback."

-> getData(function(x){
	//stop here, only execute the following after you got the data:
    getMoreData(x, function(y){
       //execute after getMoreData has gotten the data, put into y variable
  
        getMoreData(y, function(z){ 
            //so on and so forth
        });
    });
});

instead of 

   x = getData();
   y = getMoreData(x);
   z = getMoreData(y);

the reason is each part can move on a different process within nodejs

One way to allow the above is to use async

What is Async
-------------

Async is a utility module which provides straight-forward, powerful functions for working with asynchronous JavaScript. 

Async provides around 20 functions that include the usual 'functional' suspects (map, reduce, filter, each…) as well as some common patterns for asynchronous control flow (parallel, series, waterfall…). All these functions assume you follow the Node.js convention of providing a single callback as the last argument of your async function.



In a gist this is async.each:


Async.each:
// 1st para in async.each() is the array of items
  async.each(items,
  // 2nd param is the function that each item is passed to
  function(item, callback){
    // Call an asynchronous function, often a save() to DB
    item.someAsyncCall(function (){
      // Async call is done, alert via callback
      callback();
    });
  },
  // 3rd param is the function to call when everything's done
  function(err){
    // All tasks are done now
    doSomethingOnceAllAreDone();
  }
);

The function takes an array of items, then iterates over them calling a wrapper function which accepts the item as an argument. When all the calls are complete, you specify a final function to be called.

Async.parallel: The solution above works well if you simply need to iterate over a collection, but what if we have a more complex situation? 
without waiting until the previous function has complete.

The only difference is taht async actually does .each in parallel. Meaning, some of the items could finish first, some last , or both at the same time.

If you dont need 

Rather than iterating over a collection, async.parallel() allows you to push a bunch of (potentially unrelated) asynchronous calls into an array. 

Once we have the array populated, we execute all the tasks inside it, then call a function when we’re done.


Promises.
---------

One thing about promises is it allows you to flatten out nested callbacks, and is the main feature of promises that prevents "rightward drift" in programs with a lot of asynchronous code.

Errors also propagate:

  getJSON("/posts.json").then(function(posts) {

  }).catch(function(error) {
    // since no rejection handler was passed to the
   // first `.then`, the error propagates.
  });


//do something that connects to some sort of db then get from it



Sources:  
https://github.com/caolan/async


