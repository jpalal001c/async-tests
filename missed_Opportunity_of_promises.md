
					<blockquote cite="http://blog.jcoglan.com/2013/03/30/callbacks-are-imperative-promises-are-functional-nodes-biggest-missed-opportunity/">
&ldquo;So we’ve created a correct optimizing module loader with barely any code, simply by using a graph of lazy promises.

 We’ve taken the functional programming approach of using value relationships rather than explicit control flow to solve the problem, and it was much easier than if we’d written the control flow ourselves as the main element in the solution.&rdquo;
					</blockquote>
