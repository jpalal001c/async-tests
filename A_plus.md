		</ul>
						<li>Promises/A+</li>
						<ul>
							<li><a href="https://github.com/kriskowal/q">github.com/kriskowal/q</a> - The #1 choice in Node-land</li>
							<li><a href="https://github.com/tildeio/rsvp.js">github.com/tildeio/rsvp.js</a> - The most lightweight choice</li>
							<li><a href="https://github.com/cujojs/when">github.com/cujojs/when</a> - The speediest choice</li>
							<li><a href="https://github.com/promises-aplus/promises-spec/blob/master/implementations.md">github.com/promises-aplus/promises-spec/blob/master/implementations.md</a></li>
						</ul>