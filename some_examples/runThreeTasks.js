function RunThreeTasks() {
	function logError(e) {
		console.error(e);
		throw e;  // reject the Promise returned by then
	}
	var task1 = startTask1();
	var task2 = task1.then(startTask2);
	var task3 = task2.then(startTask3);
	var allTasks = task3.then(null, logError);
	return allTasks;
}