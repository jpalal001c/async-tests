var async = require('async');

async.series([
  function(cb, result){
    // code a
    console.log(result);
    return cb(null,'ABCD'); //   callback(null, 'a')
  },
  function(cb){
    console.log(cb);
    // code b
    cb(null, 'b')
  },
  function(callback){
    // code c
    callback(null, 'c')
  },
  function(callback){
    // code d
    debugger;

    callback(null, 'd')
  }],
  // optional callback
  function(err, results){
   console.log(results);    //results is now [ABCD, 'a','b']
// results is ['a', 'b', 'c', 'd']
    // final callback code
  }
)
