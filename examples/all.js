when = require('when');

var resolvedDeferred = when.defer();
resolvedDeferred.resolve([]);
var resolvedPromise = resolvedDeferred.promise;

function all(promises) {
  allPromises = promises.reduce(function(prevPromise, promise, i) {
    return prevPromise.then(function(results) {
      return promise.then(function(result) {
        results.push(result);
        return results;
      })
    });
  }, resolvedPromise);  // a Promise that resolved with []

  return allPromises;
}

function timeout(value) {
  var deferred = when.defer();
  setTimeout(function() {
    deferred.resolve(value);
  }, Math.random() * 100);
  return deferred.promise;
}

function error(value) {
  var deferred = when.defer();
  setTimeout(function() {
    deferred.reject(value);
  }, Math.random() * 100);
  return deferred.promise;
}

function showResults(results) {
  console.log('results:', results);
}

function showError(err) {
  console.error('err:', err);
}

all([timeout(1), timeout(2), timeout(4)]).then(showResults, showError);
