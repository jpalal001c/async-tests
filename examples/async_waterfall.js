//Runs the tasks array of functions in series, each passing their results to the next in the array. 
//HOWEVER, if any of the tasks pass //an error to their own callback, the next function is not executed, and the main callback is immediately called with the error.
//async 
var async = require('async');

var cards = require('./files/neutrial_minions.card.json');

async.waterfall([
    function getCardsWithDeathRattle(callback) {
    	var deathRattleCards = [];

    	if((card.effect)indexOf('DeathRattle') !== -1) {
        	deathRattleCards.push(card.health);
        }

        callback(null, deathRattleCards); //pass on the card names with deathrattle
    },
    function calculateTotalHealth(rattleCardsHealth, callback) {
      // arg1 now equals 'one' and arg2 now equals 'two'
      	var totalHealth = 0;
      	rattleCardsHealth.forEach (function(health) {
      		totalHealth += health;
      	});
        callback(null, totalHealth); //do something with cardheatl
    },
 
], function (err, result) {
	console.log(totalHealth);
    // result is totalHelth

});