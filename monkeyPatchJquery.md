					jQuery doesn't recognize Promises/A+ Promises, but all of the major Promises/A+ libraries recognize jQuery Promises:
					</p>
					<pre><code data-trim contenteditable class="javascript">
standardizedPromise = when(jQueryPromise);
					</code></pre>
					<p>
						You can even monkey-patch jQuery (overriding <code>$.Deferred</code>) to make it return standards-compliant Promises: <a href="http://jsfiddle.net/jdiamond/ZSpJX/">http://jsfiddle.net/jdiamond/ZSpJX/</a>
					</p>