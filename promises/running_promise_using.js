var promisinrequire('promising');

var Promise = require('bluebird');

module.exports = function(){
	return new Promise(function(resolve, reject){
		tradiationCallbackBasedThing(function(error, data){
			if (err) {
				reject(err);
			} else {
				resolve(data)
			}
		});
	});

}

using(getConnection(), function(connection) {
   // Don't leak the `connection` variable anywhere from here
   // it is only guaranteed to be open while the promise returned from
   // this callback is still pending
   return connection.queryAsync("SELECT * FROM TABLE");
   // Code that is chained from the promise created in the line above
   // still has access to `connection`
}).then(function(rows) {
    // The connection has been closed by now
    console.log(rows);
});
