<section>
					<h2>Dependency Graphs (with Lazy Promises)</h2>

					<pre><code data-trim contenteditable class="javascript">
// Module is defined at https://blog.jcoglan.com/2013/03/30/
new Module('getData', [], getData);
new Module('makeFolder', [], makeFolder);
new Module('writeFile', ['getData', 'makeFolder'], writeFile);
new Module('emailLink', ['writeFile'], emailLink);
					</code></pre>
				</section>